package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by asus on 2/28/2018.
 */
public class TomatoSauce extends Filling {
    Food food;

    public TomatoSauce(Food food) {
        this.food = food;
        this.description = "tomato sauce";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + this.description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.2;
    }
}
