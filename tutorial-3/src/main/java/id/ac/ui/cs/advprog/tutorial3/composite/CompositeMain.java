package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;

import java.util.List;

/**
 * Created by asus on 3/2/2018.
 */
public class CompositeMain {
    public static void main(String[] args) {
        Company myCompany = new Company();
        myCompany.addEmployee(new Ceo("Bima", 250000));
        myCompany.addEmployee(new Cto("Aldo", 150000));
        myCompany.addEmployee(new BackendProgrammer("Syah", 25000));

        System.out.println("Total Salaries  : " + myCompany.getNetSalaries());

        List<Employees> myCompanyEmployees = myCompany.getAllEmployees();

        for (Employees e : myCompanyEmployees) {
            System.out.println(e.getName() + " as " + e.getRole() + " : " + e.getSalary());
        }
    }
}
