package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by asus on 3/1/2018.
 */
public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        //TODO Implement
        if (salary < 70000) {
            throw new IllegalArgumentException("Network Expert Salary can't be less than 70000");
        }

        this.name = name;
        this.salary = salary;
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
