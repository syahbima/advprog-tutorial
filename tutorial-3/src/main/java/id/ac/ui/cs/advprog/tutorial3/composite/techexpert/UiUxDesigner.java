package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by asus on 3/1/2018.
 */
public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        //TODO Implement
        if (salary < 90000) {
            throw new IllegalArgumentException("Network Expert Salary can't be less than 90000");
        }

        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
