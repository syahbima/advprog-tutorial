package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by asus on 3/1/2018.
 */
public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
        if (salary < 100000) {
            throw new IllegalArgumentException("CTO Salary can't be less than 100000");
        }
        this.name = name;
        this.salary = salary;
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
