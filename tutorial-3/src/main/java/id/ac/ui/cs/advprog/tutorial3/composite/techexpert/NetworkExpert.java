package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by asus on 3/1/2018.
 */
public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        //TODO Implement
        if (salary < 50000) {
            throw new IllegalArgumentException("Network Expert Salary can't be less than 50000");
        }

        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
