package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by asus on 3/1/2018.
 */
public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        //TODO Implement
        if (salary < 30000) {
            throw new IllegalArgumentException("Frontend Programmer Salary can't less than 30000");
        }

        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
