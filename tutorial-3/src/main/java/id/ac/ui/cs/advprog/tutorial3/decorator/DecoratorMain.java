package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

/**
 * Created by asus on 3/2/2018.
 */
public class DecoratorMain {
    public static void main(String[] args) {
        Food bread1 = BreadProducer.THIN_BUN.createBreadToBeFilled();
        Food bread1Cheese = FillingDecorator.CHEESE.addFillingToBread(bread1);
        Food bread1CheeseBeef = FillingDecorator.BEEF_MEAT.addFillingToBread(bread1Cheese);

        System.out.println("Customer1 ordered   : " + bread1CheeseBeef.getDescription());
        System.out.println("Price   : " + bread1CheeseBeef.cost() + " dollar\n");
    }
}
