package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;


/**
 * Created by asus on 3/9/2018.
 */
public class VeggiePizzaTest {
    private Class<?> veggiePizzaClass;

    @Before
    public void setUp() throws Exception {
        String route = "id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza";
        veggiePizzaClass = Class.forName(route);
    }

    @Test
    public void testVeggiePizzaIsAPizza() {
        Class<?> parent = veggiePizzaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testVeggiePizzaOverridePrepareMethod() throws Exception {
        Method prepare = veggiePizzaClass.getDeclaredMethod("prepare");
        int methodModifiers = prepare.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", prepare.getGenericReturnType().getTypeName());
    }
}
