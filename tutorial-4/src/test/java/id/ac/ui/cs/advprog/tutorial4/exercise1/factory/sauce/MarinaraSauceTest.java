package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/9/2018.
 */


public class MarinaraSauceTest {
    private Sauce sauce;

    @Before
    public void setUp() throws Exception {
        sauce = new MarinaraSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Marinara Sauce", sauce.toString());
    }

}
