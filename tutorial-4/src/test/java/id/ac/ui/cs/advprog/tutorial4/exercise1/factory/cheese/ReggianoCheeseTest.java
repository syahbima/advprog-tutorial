package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created by asus on 3/8/2018.
 */
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese cheese;

    @Before
    public void setUp() throws Exception {
        cheese = new ReggianoCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Reggiano Cheese", cheese.toString());
    }
}
