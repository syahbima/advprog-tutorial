package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;



/**
 * Created by asus on 3/9/2018.
 */
public class PizzaTest {
    private Class<?> pizzaClass;

    @Before
    public void setUp() throws Exception {
        pizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza");
    }

    @Test
    public void testDuckIsAbstract() {
        int classModifiers = pizzaClass.getModifiers();
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testPizzaHasPrepareMethod() throws Exception {
        Method display = pizzaClass.getDeclaredMethod("prepare");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("void", display.getGenericReturnType().getTypeName());
    }


}
