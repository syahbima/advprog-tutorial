package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/9/2018.
 */
public class BlackOlivesTest {
    private Veggies veggie;

    @Before
    public void setUp() throws Exception {
        veggie = new BlackOlives();
    }

    @Test
    public void testToString() {
        assertEquals("Black Olives", veggie.toString());
    }
}
