package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;



/**
 * Created by asus on 3/9/2018.
 */
public class DepokPizzaStoreTest {
    private Class<?> depokPizzaStoreClass;

    @Before
    public void setUp() throws Exception {
        String route = "id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore";
        depokPizzaStoreClass = Class.forName(route);
    }

    @Test
    public void testDepokPizzaStoreIsAPizzaStore() {
        Class<?> parent = depokPizzaStoreClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore", parent.getName());
    }
}
