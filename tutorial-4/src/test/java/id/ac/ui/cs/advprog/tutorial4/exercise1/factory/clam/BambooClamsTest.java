package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/8/2018.
 */
public class BambooClamsTest {
    private Clams clams;

    @Before
    public void setUp() throws Exception {
        clams = new BambooClams();
    }

    @Test
    public void testToString() {
        assertEquals("Local Bamboo Clams", clams.toString());
    }
}
