package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;



/**
 * Created by asus on 3/9/2018.
 */
public class NewYorkPizzaStoreTest {
    private Class<?> newYorkPizzaStoreClass;

    @Before
    public void setUp() throws Exception {
        String route = "id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore";
        newYorkPizzaStoreClass = Class.forName(route);
    }

    @Test
    public void testNewYorkPizzaStoreIsAPizzaStore() {
        Class<?> parent = newYorkPizzaStoreClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore", parent.getName());
    }
}
