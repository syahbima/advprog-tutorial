package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/9/2018.
 */
public class CheesePizzaTest {
    private Class<?> cheesePizzaClass;

    @Before
    public void setUp() throws Exception {
        String route = "id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza";
        cheesePizzaClass = Class.forName(route);
    }

    @Test
    public void testCheesePizzaIsAPizza() {
        Class<?> parent = cheesePizzaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testCheesePizzaOverridePrepareMethod() throws Exception {
        Method prepare = cheesePizzaClass.getDeclaredMethod("prepare");
        int methodModifiers = prepare.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", prepare.getGenericReturnType().getTypeName());
    }
}
