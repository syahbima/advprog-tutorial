package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/8/2018.
 */
public class FreshClamsTest {
    private Clams clams;

    @Before
    public void setUp() throws Exception {
        clams = new FreshClams();
    }

    @Test
    public void testToString() {
        assertEquals("Fresh Clams from Long Island Sound", clams.toString());
    }
}
