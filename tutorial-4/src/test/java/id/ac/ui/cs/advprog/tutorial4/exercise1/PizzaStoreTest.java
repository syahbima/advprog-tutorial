package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/9/2018.
 */
public class PizzaStoreTest {
    private Class<?> pizzaStoreClass;

    @Before
    public void setUp() throws Exception {
        String route = "id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore";
        pizzaStoreClass = Class.forName(route);
    }

    @Test
    public void testPizzaStoreIsAbstract() {
        int classModifiers = pizzaStoreClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers));
    }
}
