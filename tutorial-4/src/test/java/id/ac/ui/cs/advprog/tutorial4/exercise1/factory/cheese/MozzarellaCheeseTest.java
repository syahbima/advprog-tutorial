package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by asus on 3/8/2018.
 */
public class MozzarellaCheeseTest {
    private MozzarellaCheese cheese;

    @Before
    public void setUp() throws Exception {
        cheese = new MozzarellaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Mozzarella", cheese.toString());
    }
}
