package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoatCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BambooClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SambalSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BambooShoot;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kale;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

/**
 * Created by asus on 3/8/2018.
 */
public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    public Dough createDough() {
        return new MediumCrustDough();
    }

    public Sauce createSauce() {
        return new SambalSauce();
    }

    public Cheese createCheese() {
        return new GoatCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BambooShoot(), new Kale(), new Spinach()};
        return veggies;
    }

    public Clams createClam() {
        return new BambooClams();
    }
}
