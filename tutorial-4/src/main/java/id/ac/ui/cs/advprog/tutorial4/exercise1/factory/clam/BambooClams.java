package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * Created by asus on 3/8/2018.
 */
public class BambooClams implements Clams {
    public String toString() {
        return "Local Bamboo Clams";
    }
}
