package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created by asus on 3/8/2018.
 */
public class GoatCheese implements Cheese {
    public String toString() {
        return "Goat Cheese";
    }
}
