package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by asus on 3/8/2018.
 */
public class BambooShoot implements Veggies {
    public String toString() {
        return "Bamboo Shoot";
    }
}
