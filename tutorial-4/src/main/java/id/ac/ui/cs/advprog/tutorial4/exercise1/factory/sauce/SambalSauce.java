package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by asus on 3/8/2018.
 */
public class SambalSauce implements Sauce {
    public String toString() {
        return "Original spicy sambal sauce";
    }
}
