package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * Created by asus on 3/8/2018.
 */
public class MediumCrustDough implements Dough {
    public String toString() {
        return "MediumCrust style crust dough";
    }
}
