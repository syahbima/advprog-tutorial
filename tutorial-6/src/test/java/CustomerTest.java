import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    Customer customer;
    Movie movie;
    Movie movie2;
    Movie movie3;
    Rental rent;
    Rental rent2;
    Rental rent3;

    public CustomerTest() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Harry Potter", Movie.REGULAR);
        movie3 = new Movie("Lord of The Rings?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        rent2 = new Rental(movie2, 1);
        rent3 = new Rental(movie3, 1);
    }

    @Test
    public void getName() {

        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }


    // TODO Implement me!
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        String result = customer.htmlStatement();
        String[] lines = result.split("\n");
        assertEquals(6, lines.length);
        assertTrue(result.contains("<p>You owe <em>7.5</em><p>"));
        assertTrue(result.contains("On this rental you earned <em>3<"
                + "/em> frequent renter points<p>"));
    }
}